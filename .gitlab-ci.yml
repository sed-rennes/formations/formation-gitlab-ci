# cf. /etc/alpine-release
# Alpine 3.6 needed to have the cppcheck package
image: alpine:3.16.2 

before_script:
- cp util/apk_repositories /etc/apk/repositories
- apk -q update && apk -q upgrade
- apk -q add gcc g++ cppunit cppunit-dev make libxslt

# By default it runs a pipeline with three stages: build, test, and deploy. You don't need to
# use all three stages; stages with no jobs are simply ignored.

#########################################################################################################
#                                                                                                       #
#                        Compilation                                                                    #
#                                                                                                       #
#########################################################################################################

job_compilation:
  stage: build
  tags:
  - ci.inria.fr
  - small
  script:
  - cd src
  - make -f Makefile_linux.mk
  - ./testbasicmath

#########################################################################################################
#                                                                                                       #
#                        Static analysis                                                                #
#                                                                                                       #
#########################################################################################################
#
# Results will be deployed with the GitLab Pages engine
# see https://sed-rennes.gitlabpages.inria.fr/formation-gitlab-ci/reports/cppcheck/
# (cppcheck-htmlreport creates an index.html file at this level)
#
job_static_analysis:
  stage: build
  tags:
  - ci.inria.fr
  - small
  script:
  - apk -q add cppcheck cppcheck-htmlreport python
  - cppcheck -j2 --quiet -f --enable=style ./src/ --xml-version=2 2> cppcheckReport.xml
  - mkdir -p public/reports/cppcheck
  - cppcheck-htmlreport --file=cppcheckReport.xml --report-dir=public/reports/cppcheck --source-dir=.
  artifacts:
    paths:
      - public/
    expire_in: 1 week


#########################################################################################################
#                                                                                                       #
#                                     Unit tests (with cppunit)                                         #
#                                                                                                       #
#########################################################################################################
#
# Results will be deployed with the GitLab Pages engine
# see https://sed-rennes.gitlabpages.inria.fr/formation-gitlab-ci/reports/cppunit/cppTestBasicMathResults.xml

job_unit_tests:
  stage: test
  tags:
  - ci.inria.fr
  - small
  script:
  - echo "Running CppUnit tests and evaluating code coverage"
  - cd src
  - make -f Makefile_linux.mk
  - ./testbasicmath
  - ls -atlrsh
  - mkdir ../public/reports/cppunit
  - cp cppTestBasicMathResults.xml ../public/reports/cppunit
# convert cppunit format into junit format
  - xsltproc ../util/cppunit2junit.xslt cppTestBasicMathResults.xml >  TestBasicMathResults.xml
  - cp TestBasicMathResults.xml ../public/reports/junit
  artifacts:
    paths:
    - src/*.xml
    - public/
    when: always
    reports:
      junit: src/TestBasicMathResults.xml


#########################################################################################################
#                                                                                                       #
#                            Evaluating code coverage (with gcov)                                       #
#                                                                                                       #
#########################################################################################################
#
# Ref. https://rachelbythebay.com/w/2011/06/01/coverage/
#
# Results will be deployed with the GitLab Pages engine
# see https://sed-rennes.gitlabpages.inria.fr/formation-gitlab-ci/reports/lcov

job_coverage:
  stage: test
  tags:
  - ci.inria.fr
  - small
  script:
  - apk -q add lcov@testing perl-gd tree
  - echo "Evaluating code coverage"
  - cd src
  - make -f Makefile_linux.mk CXXFLAGS="-O0 --coverage"
  - lcov --capture --initial --directory . --output-file .coverage.base
  - ls -atlrsh
  - ./testbasicmath
  - ls -atlrsh
  - lcov --capture --directory . --output-file .coverage.run
  - lcov --directory . --add-tracefile .coverage.base --add-tracefile .coverage.run --output-file .coverage.total
  - ls -atlrsh
  - genhtml --no-branch-coverage --output-directory lcov/ .coverage.total --highlight --frames --legend --title "Coverage test"
  - rm -f .coverage.base .coverage.run .coverage.total
  - tree lcov
  # FIXME: Tthe original line was the following
  # - cat lcov/builds/chdeltel/tpmaster/src/index.html | grep headerCovTableEntryHi | head -1 || echo OK
  - cat lcov/builds/*/src/index.html | grep headerCovTableEntryHi | head -1 || echo OK
              # the 1rst line gives the Lines coverage (the 2 line would give the Functions coverage)
              # warning : sometimes, without apparent reason, this job fails with
              #           Job failed: exit code 141
              # maybe due to head? https://stackoverflow.com/questions/19120263/why-exit-code-141-with-grep-q
  - mkdir -p ../public/reports
  - mv lcov/ ../public/reports/
  - echo "Done"
  coverage: '/^.*<td class="headerCovTableEntryHi">(\d+.\d+) %<\/td>$/'
  artifacts:
    paths:
    - src/cppTestBasicMathResults.xml
    - public/
    when: always


#########################################################################################################
#                                                                                                       #
#                             Looking for memory leaks (with valgrind)                                  #
#                                                                                                       #
#########################################################################################################
# Ref. http://valgrind.org/docs/manual/quick-start.html
#
# Results will be deployed with the GitLab Pages engine
# see https://sed-rennes.gitlabpages.inria.fr/formation-gitlab-ci/reports/valgrind/valgrind.txt

job_valgrind:
  stage: test
  tags:
  - ci.inria.fr
  - small
  script:
  - apk -q add valgrind
  - echo "Looking for memory leaks (with valgrind)"
  - cd src
  - make -f Makefile_linux.mk CXXFLAGS="-g"
  - valgrind --leak-check=yes --leak-check=full --show-leak-kinds=all ./testbasicmath &> valgrind.txt
                                    # redirects both stdout and stderr to filename
  - mkdir ../public/reports/valgrind
  - cp valgrind.txt ../public/reports/valgrind
  artifacts:
    paths:
    - src/valgrind.txt
    - public/
    when: always


#########################################################################################################
#                                                                                                       #
#                                       Deploy GitLab Pages                                             #
#                                                                                                       #
#########################################################################################################

pages:
  stage: deploy
  tags:
  - ci.inria.fr
  - small
  script:
  - echo "Updated website on $(date)"
  artifacts:
    paths:
    - public